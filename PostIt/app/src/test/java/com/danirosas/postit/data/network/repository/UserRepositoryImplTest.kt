package com.danirosas.postit.data.network.repository

import com.danirosas.postit.RxSchedulerRule
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.User
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.internal.schedulers.TrampolineScheduler
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations

class UserRepositoryImplTest{
    private val postsService: PostsService = mockk()
    private var db: PostDatabase = mockk(relaxed = true)
    private lateinit var repository: UserRepositoryImpl

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    fun `when getUser return success model`() {
        val users = arrayListOf(User("email", 123, "name", "1234567", "username", "web"))

        every { postsService.getUser(123) } returns (Observable.just(users))

        repository = UserRepositoryImpl(postsService, db)

        val testObserver = TestObserver<List<User>>()
        postsService.getUser(123).subscribeOn(TrampolineScheduler.instance()).subscribe(testObserver)

        repository.getUser(123)

        testObserver.assertValueCount(1)
        testObserver.assertValue {
            it == users
        }
    }
}