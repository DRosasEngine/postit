package com.danirosas.postit.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.danirosas.postit.data.network.repository.CommentsRepository
import com.danirosas.postit.data.network.repository.UserRepository
import com.danirosas.postit.domain.entitiy.Comment
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailPostViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var detailPostViewModel: DetailPostViewModel

    @Mock
    lateinit var commentsRepository: CommentsRepository
    @Mock
    lateinit var userRepository: UserRepository

    @Before
    fun setup(){
        detailPostViewModel = DetailPostViewModel(userRepository, commentsRepository)
    }

    @Test
    fun cantDisplayPostDetailWithoutName(){
        val name = ""
        val list = mutableListOf(Comment("body", "email.com", 1, "Juan", 1))
        val canDisplay = detailPostViewModel.canDisplayPostDetail(name, list)

        assertEquals(false, canDisplay)
    }

    @Test
    fun cantDisplayPostDetailWithoutComments(){
        val name = "Lola"
        val list = mutableListOf<Comment>()
        val canDisplay = detailPostViewModel.canDisplayPostDetail(name, list)

        assertEquals(false, canDisplay)
    }

    @Test
    fun cantDisplayPostDetailWithoutCommentsAndName(){
        val name = ""
        val list = mutableListOf<Comment>()
        val canDisplay = detailPostViewModel.canDisplayPostDetail(name, list)

        assertEquals(false, canDisplay)
    }

    @Test
    fun canDisplayPostDetail(){
        val name = "Lola"
        val list = mutableListOf(Comment("body", "email.com", 1, "Juan", 1))
        val canDisplay = detailPostViewModel.canDisplayPostDetail(name, list)

        assertEquals(true, canDisplay)
    }




    @Test
    fun getPostDetailZipped() {

    }

}