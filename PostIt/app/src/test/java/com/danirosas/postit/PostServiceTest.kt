package com.danirosas.postit

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.Comment
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.domain.entitiy.User
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


// Posts
private const val id = 1
private const val postUserId = 1
private const val title = "optio reprehenderit"
private const val body = "ut quas totam nnostrum rerum est autem sunt rem eveniet architecto"
private const val imageUrl = "http://imageexample.com"
private const val testJsonPost = """[{ "id": $id, "userId": "$postUserId", "title": "$title", "body": "$body", "postImage": "$imageUrl"}]"""

//User
private const val userId = 1
private const val userName = "Eusebio"
private const val userEmail = "Eliseo@gardner.biz"
private const val testJsonUser= """[{ "id": $userId, "name": "$userName",
    "email": "$userEmail", "phone": "", "username": "", "website": ""}]"""


//Comment
private const val commentId = 1
private const val commentPostId = 1
private const val commentName = "id labore ex et quam laborum"
private const val commentEmail = "Eliseo@gardner.biz"
private const val commentBody = "laudantium enim quasi est quidem magnam voluptate ipsam eos"
private const val testJsonComment = """[{ "id": $commentId, "postId": "$commentPostId", "name": "$commentName",
    "email": "$commentEmail", "body": "$commentBody"}]"""


@RunWith(MockitoJUnitRunner::class)
class PostServiceTestUsingMockWebServer {
    @get:Rule
    val mockWebServer = MockWebServer()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private val postService by lazy {
        retrofit.create(PostsService::class.java)
    }

    @Test
    fun getPostsEmitsPost() {
        mockWebServer.enqueue(
            MockResponse()
                .setBody(testJsonPost)
                .setResponseCode(200)
        )

        val testObserver = postService.getPosts().test()

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue {
           it ==  listOf(Post(body, id, title, postUserId, imageUrl))
        }

    }

    @Test
    fun getCommentsEmitsComments(){
        mockWebServer.enqueue(
            MockResponse()
                .setBody(testJsonComment)
                .setResponseCode(200)
        )

        val commentsList = arrayListOf(Comment(commentBody, commentEmail, commentId, commentName, commentPostId))

        val testObserver = postService.getCommentsById(commentPostId).test()

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue {
            it == commentsList
        }
    }

    @Test
    fun getUserEmitsUser(){
        mockWebServer.enqueue(
            MockResponse()
                .setBody(testJsonUser)
                .setResponseCode(200)
        )

        val testObserver = postService.getUser(1).test()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue{
            it == listOf(User(userEmail, userId, userName,"","",""))
        }
    }
}
