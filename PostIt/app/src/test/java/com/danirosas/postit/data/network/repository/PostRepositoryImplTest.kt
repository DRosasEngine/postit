package com.danirosas.postit.data.network.repository

import com.danirosas.postit.RxSchedulerRule
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.domain.entitiy.User
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.internal.schedulers.TrampolineScheduler
import io.reactivex.observers.TestObserver
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations

class PostRepositoryImplTest{
    private val postsService: PostsService = mockk()
    private var db: PostDatabase = mockk(relaxed = true)
    private lateinit var repository: PostRepositoryImpl

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    fun `when getUser return success model`() {
        val posts = arrayListOf(Post("body", 123, "title", 123, "postImage"))

        every { postsService.getPosts() } returns (Observable.just(posts))

        repository = PostRepositoryImpl(postsService, db)

        val testObserver = TestObserver<List<Post>>()
        postsService.getPosts().subscribeOn(TrampolineScheduler.instance()).subscribe(testObserver)

        repository.getPosts()

        testObserver.assertValueCount(1)
        testObserver.assertValue {
            it == posts
        }
    }
}
