package com.danirosas.postit.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.danirosas.postit.data.network.repository.PostRepositoryImpl
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.utils.testObserver
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.junit.MockitoJUnit





class MainViewModelTest{
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository: PostRepositoryImpl
    private lateinit var responseModel: List<Post>

    @Before
    fun setup(){
        repository = mockk(relaxed = true)
    }

    @Test
    fun `init method sets liveData value to empty list`() {
        val viewModel = MainViewModel(repository)

        val liveDataUnderTest = viewModel.allPosts.testObserver()

        assertEquals(0, liveDataUnderTest.observedValues.size)
    }


    @Test
    fun `when getAllPosts is success return valid data`() {
        val viewModel = MainViewModel(repository)

        val liveDataResponseModel = MutableLiveData<List<Post>>()
        liveDataResponseModel.value = listOf(Post("body", 123, "title", 123, "postImage"))

        every { repository.getPosts()} returns ( liveDataResponseModel )

        val liveDataUnderTest = viewModel.allPosts.testObserver()

        viewModel.getAllPosts()

        assertTrue(liveDataUnderTest.observedValues.size == 1)
        assertEquals(liveDataUnderTest.observedValues.first()?.first(), liveDataResponseModel.value?.get(0))
    }
}