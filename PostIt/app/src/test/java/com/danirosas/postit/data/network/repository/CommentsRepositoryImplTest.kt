package com.danirosas.postit.data.network.repository

import com.danirosas.postit.RxSchedulerRule
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.Comment
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.internal.schedulers.TrampolineScheduler
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Captor
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


class CommentsRepositoryImplTest {

    private val postsService: PostsService = mockk()
    private var db: PostDatabase = mockk(relaxed = true)
    private lateinit var repository: CommentsRepositoryImpl


    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    fun `when getPostComments return success model`() {
        val comments = arrayListOf(Comment("body", "email", 1234, "name", 123))

        every { postsService.getCommentsById(123) } returns (Observable.just(comments))

        repository = CommentsRepositoryImpl(postsService, db)

        val testObserver = TestObserver<List<Comment>>()
        postsService.getCommentsById(123).subscribeOn(TrampolineScheduler.instance()).subscribe(testObserver)

        repository.getPostComments(123)

        testObserver.assertValueCount(1)
        testObserver.assertValue {
            it == comments
        }
    }

}