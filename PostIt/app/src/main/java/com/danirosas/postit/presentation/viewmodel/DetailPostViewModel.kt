package com.danirosas.postit.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danirosas.postit.data.network.repository.CommentsRepository
import com.danirosas.postit.data.network.repository.UserRepository
import com.danirosas.postit.domain.entitiy.Comment
import com.danirosas.postit.domain.entitiy.PostDetail
import com.danirosas.postit.domain.entitiy.User
import com.danirosas.postit.extensions.zip2


class DetailPostViewModel(
    private val userRepository: UserRepository,
    private val commentsRepository: CommentsRepository
) : ViewModel() {

    private var detail = MediatorLiveData<PostDetail>()

    private val displayLiveData = MutableLiveData<Boolean>()
    fun getDisplayLiveData(): LiveData<Boolean> = displayLiveData

    fun getPostDetailZipped(
        userId: Int,
        postId: Int,
        description: String
    ): MediatorLiveData<PostDetail> {
        detail.addSource(
            zip2(userRepository.getUser(userId), commentsRepository.getPostComments(postId),
                fun(user: User, comments: List<Comment>) = Pair(user.name, comments))
        ) {
            if(it != null) {
                if (canDisplayPostDetail(it.first, it.second)) {
                    val postDetail = PostDetail(it.first, description, it.second)
                    detail.postValue(postDetail)
                    displayLiveData.postValue(true)
                }
            }else{
                displayLiveData.postValue(false)
            }
        }

        return detail
    }

    fun canDisplayPostDetail(name: String, list: List<Comment>): Boolean {
        return !(name.isEmpty() || list.isEmpty())
    }

}
