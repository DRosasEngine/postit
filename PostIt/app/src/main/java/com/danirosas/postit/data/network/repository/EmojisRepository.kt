package com.danirosas.postit.data.network.repository

interface EmojisRepository {

    // Const values
    val KEY_INFO: String
        get() = "info"
    val KEY_COUK:String
            get() = "co.uk"

    fun getUnicodeEmoji(key: String): String
}