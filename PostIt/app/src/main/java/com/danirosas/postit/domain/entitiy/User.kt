package com.danirosas.postit.domain.entitiy

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

@Entity
data class User(
    @Expose
    var email: String,
    @PrimaryKey(autoGenerate = true)
    @Expose
    val id: Int,
    @Expose
    val name: String,
    @Expose
    val phone: String,
    @Expose
    val username: String,
    @Expose
    val website: String
)