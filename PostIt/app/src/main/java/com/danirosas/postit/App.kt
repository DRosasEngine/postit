package com.danirosas.postit

import android.app.Application
import android.util.Log
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.di.startAppModule
import com.facebook.stetho.Stetho

lateinit var db: PostDatabase

class App: Application() {
    companion object {
        lateinit var INSTANCE: App
    }

    init {
        INSTANCE = this
    }

    override fun onCreate() {
        super.onCreate()
        db = PostDatabase.getInstance(this)
        INSTANCE = this

        startAppModule(this)

        initializeStetho()
    }

    private fun initializeStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }
}