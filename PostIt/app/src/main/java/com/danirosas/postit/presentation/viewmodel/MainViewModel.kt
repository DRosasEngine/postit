package com.danirosas.postit.presentation.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.danirosas.postit.data.network.repository.PostsRepository
import com.danirosas.postit.domain.entitiy.Post

class MainViewModel(private val postsRepository: PostsRepository) : ViewModel() {

    val allPosts = MediatorLiveData<List<Post>>()

    init{
        getAllPosts()
    }

    fun getSavedPosts() = allPosts

    fun getAllPosts() {
        allPosts.addSource(postsRepository.getPosts()){
            allPosts.postValue(it)
        }

    }
}
