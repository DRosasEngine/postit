package com.danirosas.postit.data.network.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.danirosas.postit.BuildConfig
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.db.dao.PostDao
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.Post
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlin.concurrent.thread

class PostRepositoryImpl(private val service: PostsService, val db: PostDatabase) :
    PostsRepository {

    private val postDao: PostDao = db.postDao()
    private var allPosts: LiveData<List<Post>>

    init {
        allPosts = postDao.getAll()
    }

    //--------------------------------------------------------------------------------------------
    //- Override methods
    //--------------------------------------------------------------------------------------------

    override fun getPosts(): LiveData<List<Post>> {

        service.getPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { parsePostsResponseModel(it) }
            .subscribeBy(
                onNext = {
                    savePosts(it)
                    Log.d(this.javaClass.simpleName, "Response: $it")

                },
                onError = {
                    Log.d(this.javaClass.simpleName, "Failure")
                }
            )

        return allPosts
    }

    private fun parsePostsResponseModel(response: List<Post>): List<Post> {
        response.forEachIndexed { index, post ->
            post.postImage = BuildConfig.RANDOM_POST_IMAGE_HOST.replace(
                "%s",
                index.toString()
            )
        }
        return (response)
    }

    override fun savePosts(posts: List<Post>) {
        thread {
            postDao.insert(posts)
        }
    }

}