
package com.danirosas.postit.presentation.view.activities

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.danirosas.postit.BuildConfig
import com.danirosas.postit.R
import com.danirosas.postit.databinding.ActivityPostDetailBinding
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.extensions.action
import com.danirosas.postit.extensions.setImageUrl
import com.danirosas.postit.extensions.snack
import com.danirosas.postit.presentation.view.adapter.CommentsAdapter
import com.danirosas.postit.presentation.viewmodel.DetailPostViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_post_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PostDetailActivity : AppCompatActivity() {

    companion object {
        const val KEY_POST = "post"
    }

    private lateinit var post: Post

    //Adapter
    private var adapter = CommentsAdapter(mutableListOf())

    //View model
    private val postDetailViewModel: DetailPostViewModel by viewModel()

    //Binding
    private lateinit var mBinding: ActivityPostDetailBinding

    //--------------------------------------------------------------------------------------------
    //- Override methods
    //--------------------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_post_detail)

        intent?.extras?.getParcelable<Post>(KEY_POST)?.let {
            post = it
        }

        commentsRecyclerView.adapter = adapter

        getUser()
        configureLiveDataObservers()
    }

    //--------------------------------------------------------------------------------------------
    //- Private methods
    //--------------------------------------------------------------------------------------------

    private fun showLoading() {
        searchProgressBar.visibility = VISIBLE
    }

    private fun hideLoading() {
        searchProgressBar.visibility = GONE
    }

    private fun getUser() {
        showLoading()

        postDetailViewModel.getPostDetailZipped(post.userId, post.id, post.body)
            .observe(this, Observer {
                if (it != null) {
                    hideLoading()
                    adapter.setComments(it.comments)

                    mBinding.detailPost = it
                    mBinding.commentsCounter.text = getString(R.string.num_comments, it.comments.size.toString())
                    mBinding.avatarUserImageView.setImageUrl(BuildConfig.AVATAR_HOST)
                } else {
                    showErrorMessage()
                }
            })
    }

    private fun showErrorMessage() {
        detail_post_layout.snack(getString(R.string.error), Snackbar.LENGTH_INDEFINITE) {
            action(getString(R.string.ok)) {
                getUser()
            }
        }
    }

    private fun configureLiveDataObservers(){
        postDetailViewModel.getDisplayLiveData().observe(this, Observer { displayed ->
            displayed?.let {
                if (!displayed) {
                    showErrorMessage()
                }
            }
        })
    }
}
