package com.danirosas.postit.presentation.view.adapter

import android.content.Context
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.danirosas.postit.BuildConfig
import com.danirosas.postit.R
import com.danirosas.postit.databinding.ItemPostBinding
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.extensions.setImageUrl

class PostListAdapter(private val posts: MutableList<Post>,
                      private var listener: (Post) -> Unit) :
    RecyclerView.Adapter<PostListAdapter.PostHolder>() {

    private lateinit var mContext: Context

    //--------------------------------------------------------------------------------------------
    //- Override methods
    //--------------------------------------------------------------------------------------------

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemPostBinding>(
            layoutInflater, R.layout.item_post,
            parent, false
        )
        mContext = parent.context

        return PostHolder(binding)
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        val post = posts[position]
        holder.binding.post = post
        holder.binding.root.setOnClickListener { listener(post) }

        holder.binding.postImageView.setImageUrl(post.postImage)

    }

    //--------------------------------------------------------------------------------------------
    //- Public methods
    //--------------------------------------------------------------------------------------------

    fun setPosts(postList: List<Post>) {
                this.posts.clear()
                this.posts.addAll(postList)
                notifyDataSetChanged()
    }

    inner class PostHolder(val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root)

}