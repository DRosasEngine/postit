package com.danirosas.postit.data.network.repository

import androidx.lifecycle.LiveData
import com.danirosas.postit.domain.entitiy.Post

interface PostsRepository {
    fun getPosts(): LiveData<List<Post>>
    fun savePosts(posts: List<Post>)
}