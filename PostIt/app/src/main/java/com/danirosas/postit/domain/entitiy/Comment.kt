package com.danirosas.postit.domain.entitiy

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Comment(
    @Expose
    val body: String,
    @Expose
    var email: String,
    @PrimaryKey(autoGenerate = true)
    @Expose
    val id: Int,
    @Expose
    val name: String,
    @Expose
    @ColumnInfo (name ="postId")
    val postId: Int
): Parcelable