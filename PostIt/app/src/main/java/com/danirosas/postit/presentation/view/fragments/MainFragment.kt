package com.danirosas.postit.presentation.view.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.danirosas.postit.R
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.extensions.action
import com.danirosas.postit.extensions.snack
import com.danirosas.postit.presentation.view.activities.PostDetailActivity
import com.danirosas.postit.presentation.view.activities.PostDetailActivity.Companion.KEY_POST
import com.danirosas.postit.presentation.view.adapter.PostListAdapter
import com.danirosas.postit.presentation.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.jetbrains.anko.intentFor


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val adapter = PostListAdapter(mutableListOf()){ post -> navigateToDetailPost(post) }

    private val viewModel: MainViewModel by viewModel()


    //--------------------------------------------------------------------------------------------
    //- Override Methods
    //--------------------------------------------------------------------------------------------

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        postsRecyclerView.adapter = adapter
        showLoading()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getPosts()
    }

    //--------------------------------------------------------------------------------------------
    //- Private methods
    //--------------------------------------------------------------------------------------------

    private fun showLoading() {
        postsRecyclerView.isEnabled = false
        progressBar.visibility = VISIBLE
    }

    private fun hideLoading(){
        postsRecyclerView.isEnabled = true
        progressBar.visibility = GONE
    }

    private fun getPosts() {
        viewModel.getSavedPosts().observe(viewLifecycleOwner, Observer { posts ->
            if(posts != null){
                hideLoading()
                adapter.setPosts(posts)
            }else{
                showMessage()
            }

        })
    }

    private fun navigateToDetailPost(post: Post) {
        val intent = Intent()
        intent.putExtra("post", post)
        activity?.let{

        startActivity(it.intentFor<PostDetailActivity>(KEY_POST to post))

        }
    }

    private fun showMessage() {
        main.snack(getString(R.string.error), Snackbar.LENGTH_INDEFINITE) {
            action(getString(R.string.ok)) {
                getPosts()
            }
        }
    }



}
