package com.danirosas.postit.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.danirosas.postit.domain.entitiy.Post

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(post: Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(post: List<Post>)

    @Query("select * from post")
    fun getAll(): LiveData<List<Post>>

    @Update
    fun updatePost(post: Post)

}