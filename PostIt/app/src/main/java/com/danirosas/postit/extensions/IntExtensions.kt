package com.danirosas.postit.extensions

fun Int.getEmojiByUnicode(): String? {
    return String(Character.toChars(this))
}