package com.danirosas.postit.domain.entitiy

data class PostDetail(var author: String, var description: String, var comments: List<Comment> ) {
}