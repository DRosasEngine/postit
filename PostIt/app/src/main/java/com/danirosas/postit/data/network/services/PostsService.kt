package com.danirosas.postit.data.network.services

import com.danirosas.postit.domain.entitiy.Comment
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.domain.entitiy.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface PostsService {

    @GET("posts")
    fun getPosts(): Observable<List<Post>>

    @GET("comments")
    fun getCommentsById(@Query("postId") postId: Int): Observable<List<Comment>>

    @GET("users")
    fun getUser(@Query("id") userId: Int): Observable<List<User>>

}