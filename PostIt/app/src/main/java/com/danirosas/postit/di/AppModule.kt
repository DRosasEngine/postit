package com.danirosas.postit.di

import android.app.Application
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.network.repository.*
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.presentation.viewmodel.DetailPostViewModel
import com.danirosas.postit.presentation.viewmodel.MainViewModel
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@JvmField
val mainViewModelModule = module {
    factory<PostsRepository> { PostRepositoryImpl(get(),get())}
    viewModel { MainViewModel(postsRepository = get()) }
}

val detailViewModelModule = module {
    factory<UserRepository> {UserRepositoryImpl(get(), get()) }
    factory<CommentsRepository> {CommentsRepositoryImpl(get(), get()) }

    viewModel { params -> DetailPostViewModel(get(), get()) }
}


val appModule = module {

    factory { provideOkHttpClient(get()) }

    factory { StethoInterceptor() }

    factory { PostDatabase.getInstance(androidApplication()) }

    single { get<PostDatabase>().postDao() }
    single { get<PostDatabase>().userDao() }
    single { get<PostDatabase>().commentDao() }

    single<PostsService> {
        val retrofit = Retrofit.Builder()
            .baseUrl(com.danirosas.postit.BuildConfig.HOST)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(provideOkHttpClient(get()))
            .build()
        retrofit.create(PostsService::class.java)
    }

}

fun provideOkHttpClient(authInterceptor: StethoInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder()
        .addNetworkInterceptor(authInterceptor)
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS).build()
}

@JvmField
val postitApp = listOf(
    appModule,
    mainViewModelModule,
    detailViewModelModule
)

fun startAppModule(application: Application) {
    startKoin {
        androidLogger(Level.DEBUG)
        androidContext(application)
        androidFileProperties()
        modules(postitApp)
    }
}
