package com.danirosas.postit.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.danirosas.postit.R
import com.danirosas.postit.databinding.ItemCommentBinding
import com.danirosas.postit.domain.entitiy.Comment


class CommentsAdapter(private val comments: MutableList<Comment>): RecyclerView.Adapter<CommentsAdapter.CommentHolder>() {

    //--------------------------------------------------------------------------------------------
    //- Override methods
    //--------------------------------------------------------------------------------------------

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemCommentBinding>(layoutInflater, R.layout.item_comment,
        parent, false)
        return CommentHolder(binding)
    }

    override fun getItemCount(): Int = comments.size

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        val comment = comments[position]

        holder.binding.comment = comment
    }

    //--------------------------------------------------------------------------------------------
    //- Public methods
    //--------------------------------------------------------------------------------------------

    fun setComments(commentsList: List<Comment>){
        comments.clear()
        comments.addAll(commentsList)
        notifyDataSetChanged()
    }

    inner class CommentHolder(val binding: ItemCommentBinding) : RecyclerView.ViewHolder(binding.root) {}

}