package com.danirosas.postit.data.db

import android.app.Application
import androidx.room.*
import com.danirosas.postit.data.db.dao.CommentDao
import com.danirosas.postit.data.db.dao.PostDao
import com.danirosas.postit.data.db.dao.UserDao
import com.danirosas.postit.domain.entitiy.Comment
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.domain.entitiy.User

@Database(entities = [(Post::class), (User::class), (Comment::class)], version = 5)
abstract class PostDatabase: RoomDatabase(){

    abstract fun postDao(): PostDao
    abstract fun userDao(): UserDao
    abstract fun commentDao(): CommentDao

    companion object {
        private val lock = Any()
        private const val DB_NAME = "PostDatabase"
        private var INSTANCE: PostDatabase? = null


        fun getInstance(application: Application): PostDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE =
                        Room.databaseBuilder(application, PostDatabase::class.java, DB_NAME)
                            .fallbackToDestructiveMigration()
                            .build()
                }
            }
            return INSTANCE!!
        }
    }
}