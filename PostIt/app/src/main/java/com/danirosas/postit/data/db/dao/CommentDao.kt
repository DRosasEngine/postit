package com.danirosas.postit.data.db.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.danirosas.postit.domain.entitiy.Comment
import com.danirosas.postit.domain.entitiy.Post
import com.danirosas.postit.domain.entitiy.User

@Dao
interface CommentDao{
    @Query("SELECT * FROM comment WHERE postId = :postId")
    fun getCommentsById(postId: Int): LiveData<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(value: List<Comment>): List<Long>

    @Query("select * from comment")
    fun getAllComments(): LiveData<List<Comment>>
}
