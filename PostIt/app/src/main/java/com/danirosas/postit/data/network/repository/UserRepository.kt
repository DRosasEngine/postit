package com.danirosas.postit.data.network.repository

import androidx.lifecycle.LiveData
import com.danirosas.postit.domain.entitiy.User

interface UserRepository {
    fun getUser(userId: Int): LiveData<User>
    fun saveUser(user: User)
}