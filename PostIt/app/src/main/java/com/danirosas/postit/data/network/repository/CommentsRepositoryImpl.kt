package com.danirosas.postit.data.network.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.Comment
import com.danirosas.postit.extensions.getEmojiByUnicode
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rx.subjects.PublishSubject
import kotlin.concurrent.thread

class CommentsRepositoryImpl(private val service: PostsService, db: PostDatabase) :
    CommentsRepository, EmojisRepository {

    private var commentDao = db.commentDao()

    val data: LiveData<List<Comment>>

    init {
        data = commentDao.getAllComments()
    }

    //--------------------------------------------------------------------------------------------
    //- Override methods
    //--------------------------------------------------------------------------------------------


    override fun getPostComments(postId: Int): LiveData<List<Comment>> {
        val comments: LiveData<List<Comment>> = commentDao.getCommentsById(postId)

        service.getCommentsById(postId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                        addEmoji(it)
                        saveComments(it)
                },
                onError = {
                    Log.d(this.javaClass.simpleName, "Failure")
                }
            )

        return comments
    }

    override fun saveComments(value: List<Comment>) {
        thread {
            commentDao.insert(value)
        }
    }

    override fun getUnicodeEmoji(key: String): String {
        return when (key) {
            KEY_INFO -> "${0x2139.getEmojiByUnicode()}${0x1FE0F.getEmojiByUnicode()}"
            KEY_COUK -> "${0x1F1EC.getEmojiByUnicode()}${0x1F1E7.getEmojiByUnicode()}"
            else -> ""
        }
    }

    //--------------------------------------------------------------------------------------------
    //- Private methods
    //--------------------------------------------------------------------------------------------

    private fun addEmoji(model: List<Comment>) {
        var key = ""
        model.forEach {
            when {
                it.email.endsWith(KEY_INFO) -> key = KEY_INFO
                it.email.endsWith(KEY_COUK) -> key = KEY_COUK
            }
            it.email = "${it.email} ${getUnicodeEmoji(key)}"
        }
    }
}