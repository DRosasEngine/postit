package com.danirosas.postit.domain.entitiy

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.danirosas.postit.BuildConfig
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Post(
    @Expose
    val body: String,
    @PrimaryKey(autoGenerate = true)
    @Expose
    val id: Int,
    @Expose
    val title: String,
    @Expose
    val userId: Int,
    @Expose
    var postImage: String
) : Parcelable