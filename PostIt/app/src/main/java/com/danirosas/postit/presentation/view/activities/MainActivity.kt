package com.danirosas.postit.presentation.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.danirosas.postit.R
import com.danirosas.postit.presentation.view.fragments.MainFragment

class MainActivity : AppCompatActivity() {

    //--------------------------------------------------------------------------------------------
    //- Override methods
    //--------------------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
    }
}
