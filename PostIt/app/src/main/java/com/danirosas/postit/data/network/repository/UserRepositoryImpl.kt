package com.danirosas.postit.data.network.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.danirosas.postit.data.db.PostDatabase
import com.danirosas.postit.data.network.services.PostsService
import com.danirosas.postit.domain.entitiy.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlin.concurrent.thread

class UserRepositoryImpl(private val service: PostsService, private val db: PostDatabase) :
    UserRepository {

    private var userDao = db.userDao()

    lateinit var user: LiveData<User>

    override fun getUser(userId: Int): LiveData<User> {
        user = getUserLiveDataFromDb(userId)

        service.getUser(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    Log.d(this.javaClass.simpleName, "Response: $it")
                    if (!it.isNullOrEmpty()) {
                        it[0].let { dataValue ->
                            if (user.value != dataValue) {
                                saveUser(dataValue)
                            }
                        }
                    } else {
                        // Return error to show in view
                        Log.d(this.javaClass.simpleName, "Success with null or empty")

                    }
                },
                onError = {
                    Log.d(this.javaClass.simpleName, "Failure")
                }
            )

        return user
    }

    fun getUserLiveDataFromDb(userId: Int) = userDao.getUser(userId)

    override fun saveUser(user: User) {
        thread {
            db.userDao().insert(user)
        }
    }
}