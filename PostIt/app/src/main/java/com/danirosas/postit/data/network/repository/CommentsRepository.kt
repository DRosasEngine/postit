package com.danirosas.postit.data.network.repository

import androidx.lifecycle.LiveData
import com.danirosas.postit.domain.entitiy.Comment

interface CommentsRepository {
    fun getPostComments(postId: Int): LiveData<List<Comment>>
    fun saveComments(comments: List<Comment>)
}